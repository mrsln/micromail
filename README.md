Micromail
=========

Микросервис для отправки почты. 

# Установка

Micromail является gb-проектом, поэтому для установки требуется только Go.
В качестве базы данных могут выступать Sqlite или Postgres.

# Конфигурация

Нужно переименовать ```config.sample.yaml``` в ```config.yaml```.

Название         | Описание
-----------------| -------------
`db.driver`      | sqlite или postgres
`db.password`    | название базы
`db.host`        | 
`db.port`        | 
`db.login`       | 
`db.password`    | 

# Запуск примера

```
gb build
cd bin
cp ../src/micromail/config.sample.yaml ./config.yaml
./server
```

```
./bin/client
```

# Сервер

Micromail использует grpc для реализации RPC и имеет метод Serve() для запуска сервера.

```
package main

import "micromail"

func main() {
  micromail.Serve() 
}
```

# Клиент

```
mail := micromail.Micromail{MailTo: "test@test.com", Subject: "test sub", Message: "test msg"}
id, err := client.Send(context.Background(), &mail)
fmt.Printf("email id: %d\n", id.Id)
```

# Методы API

Название          | Описание
-------------- ---| -------------
`Send(Micromail)` | отправляет письмо Micromail
`Status(Id)`      | присылает статус отправки письма

# TODO
  * мокать базу (github.com/erikstmartin/go-testdb)