package main

import (
  "micromail"
  "golang.org/x/net/context"
  "google.golang.org/grpc"
  "fmt"
)

const (
  serverAddr = "127.0.0.1:50051"
)

func main() {
  conn, err := grpc.Dial(serverAddr, grpc.WithInsecure())
  panicOnErr(err)
  defer conn.Close()
  client := micromail.NewSenderClient(conn)
  mail := micromail.Micromail{MailTo: "test@test.com", Subject: "test sub", Message: "test msg"}
  id, err := client.Send(context.Background(), &mail)
  panicOnErr(err)
  fmt.Printf("email id: %d\n", id.Id)
  st, err := client.Status(context.Background(), id)
  fmt.Printf("status: %d\n", st.State)
}

func panicOnErr(err error) {
  if err != nil {
    panic(err)
  }
}