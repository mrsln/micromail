package micromail

import (
  "golang.org/x/net/context"
  "testing"
  "time"
)

func TestSendAndStatus(t *testing.T) {
  mail := Micromail{}
  serv := New()
  ctx := context.Background()
  id, err := serv.Send(ctx, &mail)
  if id.Id == 0 {
    t.Fatal("Id is empty")
  }
  if err != nil {
    t.Fatal(err)
  }
  t.Logf("the id is %d\n", id.Id)
  time.Sleep(time.Second) // ждем, чтобы мок успел установить статус
  st, err := serv.Status(ctx, id)
  if err != nil {
    t.Fatal(err)
  }
  t.Logf("state is %d\n", st.State)
  if (st.State != 1) {
    t.Fatal("sending failed")
  }
}