package micromail

import (
  "log"
  "net"
  "golang.org/x/net/context"
  "google.golang.org/grpc"
    "github.com/jinzhu/gorm"
  _ "github.com/jinzhu/gorm/dialects/sqlite"
  _ "github.com/jinzhu/gorm/dialects/postgres"
)

const (
  port = ":50051"
)

type server struct {
  db *gorm.DB
}

type mailModel struct {
  gorm.Model
  MailTo string
  Subject string
  Message string
  Status State_Types
}

// New возвращает класс для обработки RPC-запросов.
// Инициализирует работу с базой.
func New() server {
  config, err := readConfig()
  if err != nil {
    panic("config panic: " + err.Error())
  }
  
  var db *gorm.DB
  if config.DB.Driver == "sqlite" {
    db, err = gorm.Open("sqlite3", "micromail.db")
  } else if config.DB.Driver == "postgres" {
    dsn := 
      " user     = " + config.DB.Login +
      " dbname   = " + config.DB.Database +
      " password = " + config.DB.Password +
      " host     = " + config.DB.Host +
      " port     = " + config.DB.Port +
      " sslmode=disable"

    db, err = gorm.Open("postgres", dsn)
  } else {
    panic("unknown driver: " + config.DB.Driver)
  }

  if err != nil {
    panic("failed to connect database")
  }

  db.AutoMigrate(&mailModel{})
  serv := server{ db: db }
  return serv
}

// Send sends a mail.
func (s *server) Send(ctx context.Context, in *Micromail) (*Id, error) {
  m := mailModel{MailTo: in.MailTo, Subject: in.Subject, Message: in.Message}
  s.db.Save(&m)
  out := Id{Id: uint32(m.ID)}

  // отправка письма
  go func() {
    sm := senderMock{}
    m.Status = sm.Send(*in)
    s.db.Save(m)
  }()

  return &out, nil
}

// Status returns a status of the mail.
func (s *server) Status(ctx context.Context, in *Id) (*State, error) {
  var mail mailModel
  s.db.First(&mail, in.Id)
  return &State{State: mail.Status}, nil
}

// Seve запускает сервер RPC.
func Serve() {
  lis, err := net.Listen("tcp", port)
  if err != nil {
    log.Fatalf("failed to listen: %v", err)
  }
  s := grpc.NewServer()
  serv := New()
  defer serv.db.Close()
  RegisterSenderServer(s, &serv)
  s.Serve(lis)
}