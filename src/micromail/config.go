package micromail

import (
  "gopkg.in/yaml.v2"
  "io/ioutil"
)

const (
  fileName = "./config.yaml"
)

// Config это структура YAML файла.
type Config struct {
  // настройки базы данных
  DB struct {
    // postgres или sqlite
    Driver string
    Host string
    Port string
    Database string
    Login string
    Password string
  }
}

func readConfig() (Config, error) {
  raw, err := ioutil.ReadFile(fileName)
  if err != nil {
    return Config{}, err
  }
  
  var c Config
  err = yaml.Unmarshal(raw, &c)
  if err != nil {
    return Config{}, err
  }
  return c, nil
}